
#!/usr/bin/python3
import paho.mqtt.client as mqtt
import logging
import sys
import requests
from time import time


log = logging.getLogger("MAIN")
log.setLevel(logging.INFO)
formatter = logging.Formatter('%(levelname)-8s %(message)s')

ch_e = logging.StreamHandler(stream=sys.stderr)
ch_e.setLevel(logging.ERROR)
ch_e.setFormatter(formatter)
log.addHandler(ch_e)

ch = logging.StreamHandler(stream=sys.stdout)
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)
log.addHandler(ch)

log.setLevel(logging.INFO)

mq_server = 'localhost'

types = {"clear": "Ясно",
         "partly-cloudy": "Малооблачно",
         "cloudy": "Облачно с прояснениями",
         "overcast": "Пасмурно",
         "partly-cloudy-and-light-rain": "Небольшой дождь",
         "partly-cloudy-and-rain": "Дождь",
         "overcast-and-rain": "Сильный дождь",
         "overcast-thunderstorms-with-rain": "Сильный дождь, гроза",
         "cloudy-and-light-rain": "Облачно, небольшой дождь",
         "overcast-and-light-rain": "Пасмурно, небольшой дождь",
         "cloudy-and-rain": "Облачно, дождь",
         "overcast-and-wet-snow": "Пасмурно, дождь со снегом",
         "partly-cloudy-and-light-snow": "Небольшой снег",
         "partly-cloudy-and-snow": "Снег",
         "overcast-and-snow": "Снегопад",
         "cloudy-and-light-snow": "Небольшой снег",
         "overcast-and-light-snow": "Небольшой снег",
         "cloudy-and-snow": "Снег"}

moons = {
    0: "Полнолуние",
    1: "Убывающая луна",
    2: "Убывающая луна",
    3: "Убывающая луна",
    4: "Последняя четверть",
    5: "Убывающая луна",
    6: "Убывающая луна",
    7: "Убывающая луна",
    8: "Новолуние",
    9: "Растущая луна",
    10: "Растущая луна",
    11: "Растущая луна",
    12: "Первая четверть",
    13: "Растущая луна",
    14: "Растущая луна",
    15: "Растущая луна"
}
winds = {
    "nw": "северо-западное",
    "n": "северное",
    "ne": "северо-восточное",
    "e": "восточное",
    "se": "юго-восточное",
    "s": "южное",
    "sw": "юго-западное",
    "w": "западное",
    "c": "Штиль"
}

API_KEY = "my-personal-api-key"

req_url = "https://api.weather.yandex.ru/v1/informers?lat={}&lon={}&lang=ru_RU".\
    format(54.123456, 37.123456)  # set your own coordinates
headers = {"X-Yandex-API-Key": "{}".format(API_KEY)}

answer = requests.get(req_url, headers=headers)
if answer.status_code == requests.codes.ok:
    weather = answer.json()
    weather_fact = weather.get('fact', {})
    weather_forecast = weather.get('forecast', {})
    condition = weather_fact.get("condition", "Неизвестно")
    wind_dir = weather_fact.get("wind_dir", "Неизвестно")
    moon_phase = weather_forecast.get("moon_code", "Неизвестно")
    day_parts = weather_forecast.get('parts', [])
    mq = mqtt.Client("Weather service")
    mq.connect(mq_server)
    mq.publish(topic="/weather/alive", payload=weather.get("now", time()))
    mq.publish(topic="/weather/temp", payload=weather_fact.get("temp", -128))
    mq.publish(topic="/weather/humidity", payload=weather_fact.get("humidity", -1))
    mq.publish(topic="/weather/temp_feels", payload=weather_fact.get("feels_like", -128))
    mq.publish(topic="/weather/wind", payload=weather_fact.get("wind_speed", -1))
    mq.publish(topic="/weather/wind_gust", payload=weather_fact.get("wind_gust", -1))
    mq.publish(topic="/weather/wind_dir", payload=winds.get(wind_dir, wind_dir))
    mq.publish(topic="/weather/pressure", payload=weather_fact.get("pressure_mm", -1))
    mq.publish(topic="/weather/condition", payload=types.get(condition, condition))
    mq.publish(topic="/weather/fc/sunrise", payload=weather_forecast.get("sunrise", '04:00'))
    mq.publish(topic="/weather/fc/sunset", payload=weather_forecast.get("sunset", '22:00'))
    mq.publish(topic="/weather/fc/moon_phase", payload=moons.get(moon_phase, moon_phase))
    for part in day_parts:
        part_name = part.get('part_name', 'unknown')
        part_condition = part.get("condition", "Неизвестно")
        wind_dir = part.get("wind_dir", "Неизвестно")
        mq.publish(topic=f"/weather/fc/{part_name}/temp_min", payload=part.get('temp_min', '-128'))
        mq.publish(topic=f"/weather/fc/{part_name}/temp_max", payload=part.get('temp_max', '-128'))
        mq.publish(topic=f"/weather/fc/{part_name}/temp_avg", payload=part.get('temp_avg', '-128'))
        mq.publish(topic=f"/weather/fc/{part_name}/temp_feels", payload=part.get('feels_like', '-128'))
        mq.publish(topic=f"/weather/fc/{part_name}/condition", payload=types.get(part_condition, part_condition))
        mq.publish(topic=f"/weather/fc/{part_name}/wind", payload=part.get("wind_speed", -1))
        mq.publish(topic=f"/weather/fc/{part_name}/wind_dir", payload=winds.get(wind_dir, wind_dir))
        mq.publish(topic=f"/weather/fc/{part_name}/wind_gust", payload=part.get("wind_gust", -1))
        mq.publish(topic=f"/weather/fc/{part_name}/humidity", payload=part.get("humidity", -1))
        mq.publish(topic=f"/weather/fc/{part_name}/prec_prob", payload=part.get("prec_prob", -1))
        mq.publish(topic=f"/weather/fc/{part_name}/prec_mm", payload=part.get("prec_mm", -1))
        mq.publish(topic=f"/weather/fc/{part_name}/pressure", payload=part.get("pressure", -1))
    mq.disconnect()
else:
    print("error: {}\nurl: {}\nheaders: {}".format(answer.status_code, req_url, headers))
